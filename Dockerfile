FROM openjdk:8-jre-alpine
COPY ./target/example.spring-boot-data-jpa.rest-controller-1.0-SNAPSHOT.jar /example.jar
WORKDIR /
EXPOSE 8080
CMD ["java", "-jar", "example.jar"]