package ack;

import ack.business.browser.domain.Cat;
import ack.business.browser.domain.CatRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import javax.transaction.Transactional;
import java.util.stream.IntStream;

@SpringBootApplication
@EnableSpringDataWebSupport
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private final CatRepository catRepository;

    public Application(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Transactional
    @Override
    public void run(String... args) {
        IntStream.range(0, 10).forEach( index ->
                catRepository.save(new Cat("puszek", "dachowiec", index + 1))
        );
    }
}
