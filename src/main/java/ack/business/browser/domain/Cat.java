package ack.business.browser.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Cat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "cat_id_generator")
    @SequenceGenerator(name = "cat_id_generator", sequenceName = "cat_id_sequence")
    private Long id;

    @Column(nullable = false)
    @NonNull
    private String name;

    @Column(nullable = false)
    @NonNull
    private String race;

    @Column(nullable = false)
    @NonNull
    private int age;

}
