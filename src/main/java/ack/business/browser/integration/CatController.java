package ack.business.browser.integration;

import ack.business.browser.domain.Cat;
import ack.business.browser.domain.CatRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/cats")
public class CatController {

    private final CatRepository catRepository;

    public CatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping("")
    public Page<Cat> getAll(Pageable pageable) {
        return catRepository.findAll(pageable);
    }

    @GetMapping("/{id}")
    public Optional<Cat> getById(@PathVariable Long id) {
        return catRepository.findById(id);
    }

}
